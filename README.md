The library does multiclass logistic regression. It has several methods:


encoded

Example: encoded(y)

We want our model to work like a library model. Therefore, we want the target variable to be one-dimensional. But in the implementation, we need the target variable in the form of one-hot. This method does just that.


eturn_probs

Example: eturn_probs(X)

This function returns us a matrix (k, m) whose element is the probability that the k-th element belongs to the m-th class.


loss

Example: loss(X, y)

This function considers an error.


grad_loss

Example: grad_loss(X, y)

This function calculates the error gradient over the weight matrix.


fit

Example: fit(self, X, y, lr=0.02, max_iter=1000)

This function is responsible for training the model using the usual gradient descent.


predict

Example: predict(X_train)

This function makes predictions.
