from LogisticRegression import MulticlassLogistic
from sklearn.metrics import accuracy_score
from sklearn.datasets import make_blobs
from sklearn.model_selection import train_test_split
import random
import numpy as np


# Для первого теста берем 2 удаленных кластера. Естественно, ожидаем
# высокого результата даже на тесте. Поэтому требуем результат не ниже 80%.
centers = [(-5, -5), (5, 5)]
cluster_std = [0.8, 1]
X, y = make_blobs(n_samples=100, cluster_std=cluster_std, centers=centers, n_features=2, random_state=1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

logistic = MulticlassLogistic()
logistic.fit(X_train, y_train)

y_pred = logistic.predict(X_test)

if accuracy_score(y_test, y_pred) < 0.8:
    raise RuntimeError('Too bad model for clusters at a big distance')

# Для второго теста берем 3 удаленных кластера. Проверяем, что для
# многоклассовой задачи модель работает а также дает хорошие результаты.

cluster_len = 100

np.random.seed(1)
X1 = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], size=cluster_len)
y1 = np.zeros(len(X1))
X2 = np.random.multivariate_normal([4, 4], [[1, 0], [0, 1]], size=cluster_len)
y2 = np.ones(len(X2))
X3 = np.random.multivariate_normal([4, -4], [[1, 0], [0, 1]], size=cluster_len)
y3 = np.ones(len(X3)) * 2
X = np.concatenate((X1, X2, X3), axis=0)
y = np.concatenate((y1, y2, y3), axis=0)

random.seed(1)
Z = list(zip(X, y))
random.shuffle(Z)
X, y = zip(*Z)
X = np.array(X)
y = np.array(y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

logistic = MulticlassLogistic()
logistic.fit(X_train, y_train)

y_pred = logistic.predict(X_test)

if accuracy_score(y_test, y_pred) < 0.8:
    raise RuntimeError('Too bad model for clusters at a big distance')

# Для третьего теста берем 2 совпадающих кластера. Тут, естественно,
# ожидаем, что результат должен быть не переобученным и не превышать 70%

centers = [(0, 0), (0, 0)]
cluster_std = [0.8, 1]
X, y = make_blobs(n_samples=100, cluster_std=cluster_std, centers=centers, n_features=2, random_state=1)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

logistic = MulticlassLogistic()
logistic.fit(X_train, y_train)

y_pred = logistic.predict(X_test)

if accuracy_score(y_test, y_pred) > 0.7:
    raise RuntimeError('Too good model for very bad data. Overfitted model?')
