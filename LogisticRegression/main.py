import numpy as np


class MulticlassLogistic:
    def __init__(self, C=None):
        self.w = None
        # X_bias = np.concatenate((X, np.ones((len(X), 1))), axis=1)
        if C is not None:
            self.C = C
        else:
            self.C = float('inf')
        self.L = np.zeros(2000)

    def encoded(self, y):
        '''
        Мы хотим, чтобы наша модель работала как библиотечная модель.
        Поэтому мы хотим, чтобы целевая переменная была одномерной.
        Но в реализации целевая переменная нам нужна в виде one-hot.
        Именно эту операцию выполняет данная функция.
        '''
        content = np.unique(y)
        dict_one_hot = {}
        for i in range(len(content)):
            a = np.zeros(len(content))
            a[i] = 1
            dict_one_hot[content[i]] = a
        encode = np.zeros((len(y), len(dict_one_hot)))
        for i in range(len(y)):
            encode[i] = dict_one_hot[y[i]]
        return encode

    def return_probs(self, X):
        '''
        Данная функция возвращает нам матрицу (k, m)-ый элемент которого есть
        вероятность того, что k-ый элемент принадлежит к m-ому классу.
        '''
        X_bias = np.concatenate((X, np.ones((len(X), 1))), axis=1)
        probs = np.zeros((np.size(X_bias, 0), np.size(self.w, 1)))
        for i in range(len(probs)):
            probs[i] = np.exp(-np.dot(X_bias[i], self.w))\
                       / np.sum(np.exp(-np.dot(X_bias[i], self.w)))
        return probs

    def loss(self, X, y):
        '''
        Данная функция считает ошибку.
        '''
        y_encoded = self.encoded(y)
        X_bias = np.concatenate((X, np.ones((len(X), 1))), axis=1)
        probs = self.return_probs(X=X)
        L = np.trace(np.dot(X_bias, np.dot(self.w, np.transpose(y_encoded))))
        for i in range(len(X_bias)):
            L += np.log(np.sum(np.exp(np.dot(-X_bias[i], self.w))))
        return L

    def grad_loss(self, X, y):
        '''
        Данная функция считает градиент ошибки по матрицу весов.
        '''
        y_encoded = self.encoded(y)
        X_bias = np.concatenate((X, np.ones((len(X), 1))), axis=1)
        return np.dot(np.transpose(X_bias), y_encoded -\
                      self.return_probs(X=X)) / len(X_bias) + self.w / self.C / len(
            X_bias)

    def fit(self, X, y, lr=0.02, max_iter=1000):
        '''
        Данная функция отвечает за обучение модели используя обычный градиентный спуск.
        '''
        y_encoded = self.encoded(y)
        if self.w is None:
            self.w = np.random.randn(np.size(X, 1) + 1, np.size(y_encoded, 1))
        for i in range(max_iter):
            self.w -= lr * self.grad_loss(X=X, y=y)
            self.L[i] = self.loss(X, y)

    def predict(self, X_test):
        '''
        Данная функция делает предсказывания.
        '''
        probs = self.return_probs(X=X_test)
        y_pred = probs.argmax(axis=1)
        return y_pred
