import os
from setuptools import setup
from setuptools import find_packages

def read_description():
    abs_path = os.path.join(os.path.dirname(__file__), "README.md")
    with open(abs_path) as f:
        return f.read()


setup(
    name="LogisticRegression",
    version="1.0",
    packages=find_packages(),
    install_requires=["numpy"],
    description="Multiclass logistic regression",
    long_description=read_description(),
    python_requires=">=3.4"
)
